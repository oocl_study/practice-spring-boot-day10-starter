package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyResponse> getAll() {
        List<Company> companyList = companyRepository.findAll();
        List<CompanyResponse> companyResponseList = CompanyMapper.toCompaniesResponse(companyList);
        return setEmployeeCount(companyResponseList);
    }

    private List<CompanyResponse> setEmployeeCount(List<CompanyResponse> companyResponseList) {
        return companyResponseList.stream().map(companyResponse -> setEmployeeCount(companyResponse.getId(), companyResponse)).collect(Collectors.toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        List<Company> companyList = companyRepository.findAll(PageRequest.of(page, pageSize)).toList();
        List<CompanyResponse> companyResponseList = CompanyMapper.toCompaniesResponse(companyList);
        return setEmployeeCount(companyResponseList);
    }

    public CompanyResponse findById(Integer companyId) {
        Company company = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        CompanyResponse companyResponse = CompanyMapper.toCompanyResponse(company);
        return setEmployeeCount(companyId, companyResponse);
    }

    public CompanyResponse create(CompanyCreateRequest companyCreateRequest) {
        Company company = CompanyMapper.toCompany(companyCreateRequest);
        Company dbCompany = companyRepository.save(company);
        CompanyResponse companyResponse = CompanyMapper.toCompanyResponse(dbCompany);
        return setEmployeeCount(dbCompany.getId(), companyResponse);
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyCreateRequest updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }
        companyRepository.save(company);

        CompanyResponse companyResponse = CompanyMapper.toCompanyResponse(company);
        return setEmployeeCount(companyId, companyResponse);
    }

    private CompanyResponse setEmployeeCount(Integer companyId, CompanyResponse companyResponse) {
        companyResponse.setEmployeeCount(employeeRepository.countByCompanyId(companyId));
        return companyResponse;
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        List<Employee> employeeList = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees();
        return EmployeeMapper.toEmployeesResponse(employeeList);
    }
}
