package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        List<Employee> allEmployee = employeeRepository.findAllByStatusTrue();
        return EmployeeMapper.toEmployeesResponse(allEmployee);
    }

    public EmployeeResponse update(int id, EmployeeCreateRequest toUpdate) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id).orElseThrow(EmployeeNotFoundException::new);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return EmployeeMapper.toEmployeeResponse(employee);
    }

    public EmployeeResponse findById(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toEmployeeResponse(employee);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        List<Employee> employeeList = employeeRepository.findByGenderAndStatusTrue(gender);
        List<EmployeeResponse> employeeResponses = EmployeeMapper.toEmployeesResponse(employeeList);
        return employeeResponses;
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        List<Employee> employeeList = employeeRepository.findAllByStatusTrue(pageRequest).toList();
        return EmployeeMapper.toEmployeesResponse(employeeList);
    }

    public EmployeeResponse insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = EmployeeMapper.toEmployee(employeeCreateRequest);
        Employee dbEmployee = employeeRepository.save(employee);
        return EmployeeMapper.toEmployeeResponse(dbEmployee);
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
