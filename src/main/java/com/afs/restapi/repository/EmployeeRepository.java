package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    List<Employee> findByGenderAndStatusTrue(String gender);

    List<Employee> findAllByStatusTrue();

    Optional<Employee> findByIdAndStatusTrue(Integer id);

    Page<Employee> findAllByStatusTrue(PageRequest pageRequest);

    List<Employee> findAllByCompanyId(Integer companyId);

    List<Employee> findAllByCompanyIdIn(List<Integer> companyIds);

    Integer countByCompanyId(Integer id);
}
